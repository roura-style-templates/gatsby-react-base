import React, { useState } from 'react'
import styled from 'styled-components'

import Botoncito from '~src/components/atoms/Botoncito'
import Input from '~src/components/atoms/Input'

const StyledInput = styled(Input)`
    margin-bottom: 12px;
`

const Container = styled.div`
    margin-top: 60px;
`

const BotonAlerta = () => {
  const [mensaje, cambiarMensaje] = useState('')

  function mostrarMensaje () {
    alert(mensaje)
  }
  return (
    <Container>
      <p>Escribí un mensaje</p>
      <StyledInput type="text" onChange={cambiarMensaje} onKeyEnter={mostrarMensaje} />
      <Botoncito onClick={mostrarMensaje}>
        Mostrar mensaje
      </Botoncito>
    </Container>
  )
}

export default BotonAlerta
